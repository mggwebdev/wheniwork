<html>
<head>
	<title>When I Work REST API</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<script src="../js/lib/jquery.min.js"></script>
	<style>
		a{
			color:blue;
			cursor:pointer;
		}
		table{
			border-collapse:collapse;
		}
		#resultsTable{
			display:none;
		}
	</style>
	<script>
		function populateUserTable(results){
			$('#usersTBody').html('');
			for(var i=0;i<results.length;i++){
				$('#usersTBody').append('<tr data-id="'+results[i]['id']+'">'+
					'<td>'+results[i]['id']+'</td>'+
					'<td>'+results[i]['name']+'</td>'+
					'<td>'+results[i]['role']+'</td>'+
					'<td>'+results[i]['email']+'</td>'+
					'<td>'+results[i]['phone']+'</td>'+
					'<td>'+(results[i]['created_at']?new Date(Number(results[i]['created_at'])):'')+'</td>'+
					'<td>'+(results[i]['updated_at']?new Date(Number(results[i]['updated_at'])):'')+'</td>'+
				'</tr>');
			}
		}
		function refreshUsersTable(params){
			var id=params.id || null;
			$.ajax({
				type:'GET',
				url:'rest.php',
				data:{
					params:{
						id:id
					},
					action:'getUsers'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							populateUserTable(response.results);
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		}
		function populateShiftTable(results){
			$('#shiftTBody').html('');
			for(var i=0;i<results.length;i++){
				$('#shiftTBody').append('<tr data-id="'+results[i]['id']+'">'+
					'<td>'+results[i]['id']+'</td>'+
					'<td>'+results[i]['manager_id']+'</td>'+
					'<td>'+results[i]['employee_id']+'</td>'+
					'<td>'+results[i]['break']+'</td>'+
					'<td>'+(results[i]['start_time']?new Date(Number(results[i]['start_time'])):'')+'</td>'+
					'<td>'+(results[i]['end_time']?new Date(Number(results[i]['end_time'])):'')+'</td>'+
					'<td>'+(results[i]['created_at']?new Date(Number(results[i]['created_at'])):'')+'</td>'+
					'<td>'+(results[i]['updated_at']?new Date(Number(results[i]['updated_at'])):'')+'</td>'+
				'</tr>');
			}
		}
		function refreshShiftsTable(params){
			var id=params.id || null;
			$.ajax({
				type:'GET',
				url:'rest.php',
				data:{
					params:{
						id:id
					},
					action:'getShifts'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							populateShiftTable(response.results);
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		}
		function populateResultsTable(results){
			$('#resultsTBody,#dataHeadRow').html('');
			var str,val;
			for(var i=0;i<results.length;i++){
				str='<tr data-id="'+results[i]['id']+'">'
				for(var key in results[i]){
					if(!i){
						$('#dataHeadRow').append('<th>'+key+'</th>')
					}
					val=results[i][key];
					if(val && (key=='created_at' || key=='updated_at' || key=='start_time' || key=='end_time')){val=new Date(Number(val));}
					str+='<td>'+val+'</td>';
				}
				str+='</tr>';
				$('#resultsTBody').append(str);
			}
		}
		function getData(params,action){
			$.ajax({
				type:'POST',
				url:'rest.php',
				data:{
					params:params,
					action:action
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(params.table=='user'){
								populateUserTable(response.results);
							}else if(params.table=='shift'){
								populateShiftTable(response.results);
							}else if(params.table=='results'){
								populateResultsTable(response.results);
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		}
		$(document).ready(function(){
			// refreshUsersTable({});
			// refreshShiftsTable({});
			$('#getAllUsers').click(function(){
				refreshUsersTable({});
			});
			$('#getAllShifts').click(function(){
				refreshShiftsTable({});
			});
			$('body').on('click','#getMyShifts',function(){
				getData({table:'results',id:$('#employeeID3').val()},'getShifts');
				$('#container1').append($('#resultsTable').css('display','block'));
			});
			$('body').on('click','#whoAmIWorkingWith',function(){
				getData({table:'results',id:$('#employeeID4').val(),shiftid:$('#shiftID3').val()},'getRelatedShiftsFromMyTime');
				$('#container2').append($('#resultsTable').css('display','block'));
			});
			$('body').on('click','#getMyHoursByWeek',function(){
				getData({table:'results',id:$('#employeeID5').val()},'getHoursPerWeek');
				$('#container3').append($('#resultsTable').css('display','block'));
			});
			$('body').on('click','#getManagersContact',function(){
				getData({table:'results',id:$('#employeeID6').val(),shiftid:$('#shiftID4').val()},'getManagersInfoFromShifts');
				$('#container4').append($('#resultsTable').css('display','block'));
			});
			$('body').on('click','#createShift',function(){
				if($('#startDateC').val() && $('#startTimeC').val() && $('#endDateC').val() && $('#endTimeC').val()){
					getData({table:'results',start_time:new Date($('#startDateC').val()+' '+$('#startTimeC').val()).getTime(),end_time:new Date($('#endDateC').val()+' '+$('#endTimeC').val()).getTime(),shiftid:$('#shiftID').val(),created_at:new Date().getTime(),manager_id:$('#managerIDC').val(),employee_id:$('#employeeIDC').val(),break:$('#breakC').val()},'createShift');
					$('#container5').append($('#resultsTable').css('display','block'));
				}
			});
			$('body').on('click','#getShiftsFromTimePeriod',function(){
				getData({table:'results',start_time:new Date($('#startDate').val()+' '+$('#startTime').val()).getTime(),end_time:new Date($('#endDate').val()+' '+$('#endTime').val()).getTime()},'getShiftsFromTimePeriod');
				$('#container6').append($('#resultsTable').css('display','block'));
			});
			$('body').on('click','#updateShift',function(){
				if($('#startDate2').val() && $('#startTime2').val() && $('#endDate2').val() && $('#endTime2').val()){
					getData({table:'results',start_time:new Date($('#startDate2').val()+' '+$('#startTime2').val()).getTime(),end_time:new Date($('#endDate2').val()+' '+$('#endTime2').val()).getTime(),shiftid:$('#shiftID').val()},'updateShift');
					$('#container7').append($('#resultsTable').css('display','block'));
				}
			});
			$('body').on('click','#addEmployeeToShift',function(){
				getData({table:'results',employee_id:$('#employeeID').val(),shiftid:$('#shiftID2').val()},'updateShift');
					$('#container8').append($('#resultsTable').css('display','block'));
			});
			$('body').on('click','#getEmployeeDetails',function(){
				getData({table:'results',id:$('#employeeID2').val()},'getUsers');
				$('#container9').append($('#resultsTable').css('display','block'));
			});
		});
	</script>
</head>
<body>
	<h1>When I Work REST API</h1>
	<table id="usersTable" border="1">
		<thead>
			<tr>
				<th colspan="99999">All Users</th>
			</tr>
			<tr>
				<th>User ID</th>
				<th>Name</th>
				<th>Role</th>
				<th>E-Mail</th>
				<th>Phone</th>
				<th>Created Time</th>
				<th>Updated Time</th>
			</tr>
		</thead>
		<tbody id="usersTBody">
		</tbody>
	</table>
	<div class="bottomButtons">
		<a id="getAllUsers">Get All Users</a><br/>
	</div>
	<table id="shifTable" border="1">
		<thead>
			<tr>
				<th colspan="99999">All Shifts</th>
			</tr>
			<tr>
				<th>Shift ID</th>
				<th>Manager ID</th>
				<th>Employee ID</th>
				<th>Break</th>
				<th>Start Time</th>
				<th>End Time</th>
				<th>Created Time</th>
				<th>Updated Time</th>
			</tr>
		</thead>
		<tbody id="shiftTBody">
		</tbody>
	</table>
	<div class="bottomButtons">
		<a id="getAllShifts">Get All Shifts</a>
	</div>
	<table id="resultsTable" border="1">
		<thead>
			<tr>
				<th colspan="9999">Results</th>
			</tr>
			<tr id="dataHeadRow">
			</tr>
		</thead>
		<tbody id="resultsTBody">
		</tbody>
	</table>
	<div id="container1">
		<h3>Get My Shifts</h3>
		<span>Employee ID:</span><input type="text" id="employeeID3" value="2"/>
		<a id="getMyShifts">Get My Shifts</a>
	</div>
	<div id="container2">
		<h3>Who Am I Working With</h3>
		<span>Employee ID:</span><input type="text" id="employeeID4" value="2"/><br/>
		<span>Shift ID:</span><input type="text" id="shiftID3" value="1"/>
		<a id="whoAmIWorkingWith">Who Am I working With</a>
	</div>
	<div id="container3">
		<h3>Get My Hours By Week</h3>
		<span>Employee ID:</span><input type="text" id="employeeID5" value="2"/>
		<a id="getMyHoursByWeek">Get my Hours By Week</a>
	</div>
	<div id="container4">
		<h3>Get My Managers' Conact Info</h3>
		<span>Employee ID:</span><input type="text" id="employeeID6" value="2"/>
		<a id="getManagersContact">Get My Managers' Conact Info</a>
	</div>
	<div id="container5">
		<h3>Create Shift</h3>
		<span>Manager ID:</span><input type="text" id="managerIDC" value="1"/><br/>
		<span>Employee ID:</span><input type="text" id="employeeIDC" value="2"/><br/>
		<span>Break:</span><input type="text" id="breakC" value="15"/><br/>
		<span>Start Date:</span><input type="date" id="startDateC"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Start Time:</span><input type="time" id="startTimeC"/><br/><span>End Date:</span><input type="date" id="endDateC"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>End Time:</span><input type="time" id="endTimeC"/>
		<a id="createShift">Get Shifts From Time Period</a>
	</div>
	<div id="container6">
		<h3>Get Shift From Time Period</h3>
		<span>Start Date:</span><input type="date" id="startDate"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Start Time:</span><input type="time" id="startTime"/><br/><span>End Date:</span><input type="date" id="endDate"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>End Time:</span><input type="time" id="endTime"/>
		<a id="getShiftsFromTimePeriod">Get Shifts From Time Period</a>
	</div>
	<div id="container7">
		<h3>Update Shift</h3>
		<span>Shift ID:</span><input type="text" id="shiftID"/><br/>
		<span>Start Date:</span><input type="date" id="startDate2"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Start Time:</span><input type="time" id="startTime2"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>End Date:</span><input type="date" id="endDate2"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>End Time:</span><input type="time" id="endTime2"/>
		<a id="updateShift">Update Shift</a>
	</div>
	<div id="container8">
		<h3>Add Emplyee to Current Shift</h3>
		<span>Employee ID:</span><input type="text" id="employeeID" value="4"/><br/>
		<span>Shift ID:</span><input type="text" id="shiftID2" value="3"/>
		<a id="addEmployeeToShift">Add Emplyee to Current Shift</a>
	</div>
	<div id="container9">
		<h3>Get Employee Details</h3>
		<span>Employee ID:</span><input type="text" id="employeeID2" value="2"/>
		<a id="getEmployeeDetails">Get Employee Details</a>
	</div>
</body>
</html>