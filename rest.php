<?php
	require('functions.php');
	if(isset($_POST['action']) || isset($_GET['action'])){
		if(isset($_POST['action'])){
			$data=$_POST;
		}elseif(isset($_GET['action'])){
			$data=$_GET;
		}
		call_user_func($data['action'],$data['params']);
	}
	function getShifts($params){
		$con=connectToDB();
		$where="";
		if(isset($params['id']) && $params['id']){
			$where=" WHERE employee_id=".$params['id'];
		}
		if(isset($params['shiftid']) && $params['shiftid']){
			if($where){$where.=" AND ";
			}else{$where.=" WHERE ";}
			$where.="id=".$params['shiftid'];
		}
		$sql="SELECT * FROM `shift`".$where." ORDER BY start_time ASC";
		$sqlPreped=$con->prepare($sql);
		$response=new stdClass();
		$response->params=$params;
		if($sqlPreped->execute()){
			$response->results=$sqlPreped->fetchAll(PDO::FETCH_ASSOC);
			$response->status="OK";
		}else{
			$response->status="ERROR";
			$response->msg=$con->errorInfo();
		}
		echo json_encode($response);
	}
	function getUsers($params){
		$con=connectToDB();
		$where="";
		if(isset($params['id']) && $params['id']){
			$where=" WHERE id=".$params['id'];
		}
		$sql="SELECT * FROM `user`".$where." ORDER BY id ASC";
		$sqlPreped=$con->prepare($sql);
		$response=new stdClass();
		$response->params=$params;
		if($sqlPreped->execute()){
			$response->results=$sqlPreped->fetchAll(PDO::FETCH_ASSOC);
			$response->status="OK";
		}else{
			$response->status="ERROR";
			$response->msg=$con->errorInfo();
		}
		echo json_encode($response);
	}
	function getRelatedShiftsFromMyTime($params){
		$con=connectToDB();
		$sql="SELECT * FROM `shift` WHERE (
			start_time BETWEEN (SELECT start_time FROM `shift` WHERE id=".$params['shiftid'].") AND (SELECT end_time FROM `shift` WHERE id=".$params['shiftid'].") OR 
			end_time BETWEEN (SELECT start_time FROM `shift` WHERE id=".$params['shiftid'].") AND (SELECT end_time FROM `shift` WHERE id=".$params['shiftid'].") OR 
			(SELECT start_time FROM `shift` WHERE id=".$params['shiftid'].") BETWEEN  start_time AND end_time) AND id!=".$params['shiftid']." ORDER BY start_time ASC";
		$sqlPreped=$con->prepare($sql);
		$response=new stdClass();
		$response->params=$params;
		if($sqlPreped->execute()){
			$response->results=$sqlPreped->fetchAll(PDO::FETCH_ASSOC);
			$response->status="OK";
		}else{
			$response->status="ERROR";
			$response->msg=$con->errorInfo();
		}
		echo json_encode($response);
	}
	function getHoursPerWeek($params){
		$con=connectToDB();
		$sql="SELECT CONCAT(YEAR(DATE_FORMAT(FROM_UNIXTIME(start_time/1000),'%Y-%m-%d')),'/',WEEK(DATE_FORMAT(FROM_UNIXTIME(start_time/1000),'%Y-%m-%d'))) AS week_name, count(*) as shift_count from shift WHERE employee_id=".$params['id']." GROUP BY week_name ORDER BY start_time ASC";
		$sqlPreped=$con->prepare($sql);
		$response=new stdClass();
		$response->params=$params;
		if($sqlPreped->execute()){
			$response->results=$sqlPreped->fetchAll(PDO::FETCH_ASSOC);
			$response->status="OK";
		}else{
			$response->status="ERROR";
			$response->msg=$con->errorInfo();
		}
		echo json_encode($response);
	}
	function getManagersInfoFromShifts($params){
		$con=connectToDB();
		$sql="SELECT * FROM user where id IN (SELECT manager_id FROM shift WHERE employee_id=".$params['id'].") ORDER BY id";
		$sqlPreped=$con->prepare($sql);
		$response=new stdClass();
		$response->params=$params;
		if($sqlPreped->execute()){
			$response->results=$sqlPreped->fetchAll(PDO::FETCH_ASSOC);
			$response->status="OK";
		}else{
			$response->status="ERROR";
			$response->msg=$con->errorInfo();
		}
		echo json_encode($response);
	}
	function getManagerContactInfoFromShift($params){
		$con=connectToDB();
		$sql="SELECT * FROM user where id=(SELECT manager_id FROM shift WHERE id=".$params['id'].") ORDER BY id";
		$sqlPreped=$con->prepare($sql);
		$response=new stdClass();
		$response->params=$params;
		if($sqlPreped->execute()){
			$response->results=$sqlPreped->fetchAll(PDO::FETCH_ASSOC);
			$response->status="OK";
		}else{
			$response->status="ERROR";
			$response->msg=$con->errorInfo();
		}
		echo json_encode($response);
	}
	function createShift($params){
		$con=connectToDB();
		$sql="INSERT INTO shift (manager_id,employee_id,break,start_time,end_time,created_at) VALUES (".$params['manager_id'].",".$params['employee_id'].",".$params['break'].",".$params['start_time'].",".$params['end_time'].",".$params['created_at'].")";
		$sqlPreped=$con->prepare($sql);
		$response=new stdClass();
		$response->params=$params;
		if($sqlPreped->execute()){
			$params['shiftid']=$con->lastInsertId();
			getShifts($params);
			return;
			$response->status="OK";
		}else{
			$response->status="ERROR";
			$response->msg=$con->errorInfo();
		}
		echo json_encode($response);
	}
	function getShiftsFromTimePeriod($params){
		$con=connectToDB();
		$sql="SELECT * FROM shift WHERE (
			start_time BETWEEN ".$params['start_time']." AND ".$params['end_time']." OR 
			end_time BETWEEN ".$params['start_time']." AND ".$params['end_time']." OR 
			".$params['start_time']." BETWEEN  start_time AND end_time) ORDER BY start_time";
		$sqlPreped=$con->prepare($sql);
		$response=new stdClass();
		$response->params=$params;
		if($sqlPreped->execute()){
			$response->results=$sqlPreped->fetchAll(PDO::FETCH_ASSOC);
			$response->status="OK";
		}else{
			$response->status="ERROR";
			$response->msg=$con->errorInfo();
		}
		echo json_encode($response);
	}
	function updateShift($params){
		$con=connectToDB();
		$udpateStr="";
		if(isset($params['start_time']) && $params['start_time']){
			$udpateStr="start_time=".$params['start_time'];
		}
		if(isset($params['end_time']) && $params['end_time']){
			if($udpateStr){$udpateStr.=", ";}
			$udpateStr.="end_time=".$params['end_time'];
		}
		if(isset($params['employee_id']) && $params['employee_id']){
			if($udpateStr){$udpateStr.=", ";}
			$udpateStr.="employee_id=".$params['employee_id'];
		}
		$response=new stdClass();
		$response->params=$params;
		if(!$udpateStr){
			$response->status="ERROR";
			$response->msg="No Update Data.";
			echo json_encode($response);
			exit;
		}
		$sql="UPDATE shift SET ".$udpateStr." WHERE id=".$params['shiftid'];
		$sqlPreped=$con->prepare($sql);
		if($sqlPreped->execute()){
			getShifts($params);
			return;
			$response->status="OK";
		}else{
			$response->status="ERROR";
			$response->msg=$con->errorInfo();
		}
		echo json_encode($response);
	}
?>
