<?php
	require('config.php');
	function connectToDB(){
		GLOBAL $host, $user, $pass, $db;
		try {
			return new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
		} catch (PDOException $e) {
			echo "Error:".$e->getMessage()."<br>";
			die();
		}
	}
?>
